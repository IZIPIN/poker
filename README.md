# Абазар
***
*Игра в классический покер для одного человека. Написана на языке программирования Python версии 3.9.1*
***
### Состав проекта
* *main.py* - Основной файл проекта
* *combo.py* - Файл определения удачной комбинации
* *classes* - Файл с классами, используемыми в основном файле
***
### Внешний вид
#### Изображение главного меню
![menu](https://sun9-35.userapi.com/impg/XC7z8oQQJAGNRd6263xt_oAMIYLwd_32_btA5g/WyeeUWCdSCU.jpg?size=1498x913&quality=96&sign=6326ec5905772a850cc2d4fabb2f7b5e&type=album)
***
#### Изображение геймплея
![gameplay](https://sun9-79.userapi.com/impg/OurbigutOGLtf1gBMoshJ_DD6qlfEgM8zdDqdA/hMepDYCfKVc.jpg?size=1198x729&quality=96&sign=43c90430613c67af7a1173bd24d2482d&type=album)
***
### Использованные библиотеки
* [pygame](https://pypi.org/project/pygame/)
* [tkinter](https://docs.python.org/3/library/tkinter.html)
* [random](https://docs.python.org/3/library/random.html)
* [pygame_menu](https://pygame-menu.readthedocs.io/en/4.1.3/)

