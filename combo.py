def max_cards(lists):
    def_cards = lists.copy()
    # [[card, card, ...], [card, cards, ...], ...]
    # | .value = const |  | .value = const |
    maxim = 0
    for row in def_cards:
        val = from_str_to_int([row[0].value])[0]
        if val > maxim:
            maxim = val
    maxim = from_int_to_str(maxim)
    for row in def_cards:
        if row[0].value == maxim:
            return row


def extra(card, k1):
    """вынимаем"""
    def_cards = card
    k = k1
    b = []
    ind = 0
    lena = len(def_cards)
    while ind < lena:
        if def_cards[ind].value == k:
            b.append(def_cards[ind])
            def_cards = def_cards[:ind] + def_cards[ind + 1:]
            lena -= 1
            continue
        ind += 1
    return def_cards, b


def val_sui_split(lists):
    def_cards = lists
    """вынимаем значения и масти карт"""
    def_suits = []
    nums = []
    for card in def_cards:
        def_suits.append(card.suit)
        nums.append(card.value)
    return nums, def_suits


def from_str_to_int(num):
    """переходим от букв к цифрам"""
    nums = num
    # 2 3 4 5 6 7 8 9 10  J  Q  K  A
    # 2 3 4 5 6 7 8 9 10 11 12 13 14
    for i in range(len(nums)):
        if nums[i] == 'J':
            nums[i] = 11
        elif nums[i] == 'Q':
            nums[i] = 12
        elif nums[i] == 'K':
            nums[i] = 13
        elif nums[i] == 'A':
            nums[i] = 14
        nums[i] = int(nums[i])
    return nums


def from_int_to_str(num):
    """переходим от цифк к буквам"""
    # 2 3 4 5 6 7 8 9 10  J  Q  K  A
    # 2 3 4 5 6 7 8 9 10 11 12 13 14
    if num == 11:
        num = 'J'
    elif num == 12:
        num = 'Q'
    elif num == 13:
        num = 'K'
    elif num == 14:
        num = 'A'
    num = str(num)

    return num


def higest_card(card_row):  # принимает cards
    """комбинация одной высшей карты"""
    def_cards = card_row.copy()
    bet_cards = []
    for card in def_cards:
        bet_cards.append([card])
    max_val = max_cards(bet_cards)[0]
    def_cards.remove(max_val)
    return max_val, def_cards


def if_flesh(lists):  # принимает cards
    """проверка на флеш (5 карт одной масти)
    принимает от 5 до 7 карт"""
    def_cards = lists
    nums, def_suits = val_sui_split(def_cards)
    """заполняем словарь по мастям"""
    dikt = {}
    for sui in def_suits:
        if sui in dikt.keys():
            dikt[sui] += 1
        else:
            dikt[sui] = 1
    mast = None
    is_flash = False
    """ищем 5 одинаковых мастей"""
    for k, v in dikt.items():
        if v >= 5:
            mast = k
            is_flash = True
    flash_arr = []
    b = []
    if is_flash:
        ind = 0
        lena = len(def_cards)
        while ind < lena:
            if def_cards[ind].suit == mast:
                b.append(def_cards[ind])
                def_cards = def_cards[:ind] + def_cards[ind + 1:]
                lena -= 1
                continue
            ind += 1
        for i in range(5):
            maxim, b = higest_card(b)
            flash_arr.append(maxim)
        def_cards.extend(b)
    return is_flash, flash_arr, def_cards


def simple_street(num):
    nums = num
    """сортировка по убыванию"""
    nums.sort(reverse=True)
    """ищем 5 подряд"""
    ind = None
    is_street = False
    max_val = None
    for j in range(len(num) - 4):
        streak = 0
        ind = j
        end = j + 4
        k = j
        while k < end:
            """выход из цикла при выходе за размер списка"""
            if k == len(num) - 1:
                break
            """обработка одинаковых подряд"""
            if num[k] == num[k+1]:
                end += 1
                k += 1
                continue
            if abs(num[k] - num[k+1]) == 1:
                streak += 1
            k += 1

        if streak == 4:
            is_street = True
            break
    if is_street:
        max_val = nums[ind]
    else:
        max_val = -1
    return is_street, max_val


def if_street(lists):  # принимает cards
    """проверка на стрит (5 карт по порядку)
    принимает от 5 до 7 карт"""
    def_cards = lists
    nums, def_suits = val_sui_split(def_cards)
    nums = from_str_to_int(nums)

    is_street = False
    max_street = -1
    is_street, max_street = simple_street(nums)
    if not is_street:
        max_street = -1

    """если есть туз, то он может быть еще и в конце"""
    nums2 = []
    loc_street = False
    loc_max = -1
    if 14 in nums:
        nums2 = nums
        for i in range(len(nums2)):
            if nums2[i] == 14:
                nums2[i] = 1
        loc_street, loc_max = simple_street(nums2)
    """ставим максимальный стрит если он есть"""
    if loc_max > max_street:
        is_street = loc_street
        max_street = loc_max
    """если стрит есть, то делаем список списков"""
    street_arr = []
    if is_street:
        row_for = range(max_street, max_street-5, -1)
        for val in range(max_street, max_street-5, -1):
            k = from_int_to_str(val)
            if loc_street:
                if k == '1':
                    k = 'A'

            # TODO: говно вот тут
            def_cards, d = extra(def_cards, k)

            if len(d) > 0:
                def_cards.extend(d[1:])
                d = d[0]
            street_arr.append(d)

    return is_street, street_arr, def_cards


def complicated_ones(lists):
    """проверка на флеши и стриты """
    def_cards = lists
    is_street, street_arr, not_s_cards = if_street(def_cards)
    is_flesh, flesh_arr, not_f_cards = if_flesh(def_cards)

    if is_flesh:
        is_f_s, f_s_arr, not_f_s_cards = if_street(flesh_arr)
        if is_f_s:
            """стрит-флеш"""
            maxim, non_maxim = higest_card(f_s_arr)
            maxim2, non_maxim = higest_card(non_maxim)
            if maxim.value == 'A' and maxim2.value == 'K':
                """роял"""
                return 9, flesh_arr, 'роял стрит-флеш', not_f_cards
            else:
                """не роял"""
                return 8, flesh_arr, 'стрит-флеш', not_f_cards
        else:
            """флеш"""
            return 5, flesh_arr, 'флеш', not_f_cards
    else:
        """не флеш"""
        if is_street:
            """стрит"""
            return 4, street_arr, 'стрит', not_s_cards
        else:
            """ничего, максимум старшая карта"""
            hig, def_cards = higest_card(def_cards)
            return 0, hig, 'высшая карта', def_cards


def simple_ones(lists):  # принимает cards
    def_cards = lists
    nums, def_suits = val_sui_split(def_cards)
    """заполнение словаря со значениями"""
    dikt = {}
    for j in nums:
        if j in dikt.keys():
            dikt[j] += 1
        else:
            dikt[j] = 1
    # print(dikt)

    """проверка значений"""
    if 4 in dikt.values():
        """выявляем каре"""
        maxim = None
        for k, v in dikt.items():
            if v == 4:
                """вынимаем каре"""
                def_cards, maxim = extra(def_cards, k)
        # print('yes', maxim, 'каре')
        return 7, maxim, 'каре', def_cards

    if 3 in dikt.values() and 2 in dikt.values():
        """выявляем фулл хаус"""
        tri = None
        dva = []
        for k, v in dikt.items():
            if v == 3:
                """вынимаем фулл хаус"""
                def_cards, tri = extra(def_cards, k)
            if v == 2:
                """вынимаем фулл хаус"""
                def_cards, d = extra(def_cards, k)
                dva.append(d)

        # print('yes', tri, max(dva), 'фулл хаус')
        return 6, (tri, max_cards(dva)), 'фулл хаус', def_cards
    if list(dikt.values()).count(3) == 2:
        """выявляем сет, если две тройки"""
        tri = []
        for k, v in dikt.items():
            if v == 3:
                """вынимаем сет"""
                def_cards, d = extra(def_cards, k)
                tri.append(d)
        # print('yes', max(tri), 'сет')
        return 3, max_cards(tri), 'сет', def_cards
    if list(dikt.values()).count(2) == 3:
        """выявляем две пары, если три двойки"""
        dva = []
        for k, v in dikt.items():
            if v == 2:
                def_cards, d = extra(def_cards, k)
                dva.append(d)
        maxim1 = max_cards(dva)
        dva.remove(maxim1)
        maxim2 = max_cards(dva)
        # print('yes', maxim1, maxim2, 'две пары')
        return 2, maxim1 + maxim2, 'две пары', def_cards

    """обрабатываем более сложные комбинации
     где могут быть не только выше перечисленные комбинации"""
    local_komb = None
    local_karta = None
    local_name = None
    local_cards = None
    if list(dikt.values()).count(3) == 1:
        """выявляем сет, если одна тройка"""
        tri = None
        l_cards = def_cards.copy()
        for k, v in dikt.items():
            if v == 3:
                l_cards, d = extra(l_cards, k)
                tri = d
        # print('yes', tri, 'сет')
        local_komb, local_karta, local_name, local_cards = 3, tri, 'сет', l_cards
    if list(dikt.values()).count(2) == 2:
        """выявляем две пары, если две двойки"""
        dva = []
        l_cards = def_cards.copy()
        for k, v in dikt.items():
            if v == 2:
                l_cards, d = extra(l_cards, k)
                dva.append(d)
        maxim1 = max_cards(dva)
        dva.remove(maxim1)
        maxim2 = max_cards(dva)
        # print('yes', maxim1, maxim2, 'две пары')
        local_komb, local_karta, local_name, local_cards = 2, maxim1 + maxim2, 'две пары', l_cards
    if list(dikt.values()).count(2) == 1:
        """выявляем пару, если одна двойка"""
        dva = None
        l_cards = def_cards.copy()
        for k, v in dikt.items():
            if v == 2:
                l_cards, d = extra(l_cards, k)
                dva = d
        # print('yes', dva, 'пара')
        local_komb, local_karta, local_name, local_cards = 1, dva, 'пара', l_cards
    if len(dikt) == len(def_cards):
        # if len(dikt) == len(nums):
        l_cards = def_cards.copy()
        """все карты разные"""
        maxim, l_cards = higest_card(l_cards)

        local_komb, local_karta, local_name, local_cards = 0, maxim, 'высшая карта', l_cards

    if len(dikt.values()) < 5:
        """если меньше 5 разных карт, то возвращаем то, что нашли"""
        return local_komb, local_karta, local_name, local_cards
    else:
        """если больше 5 разных карт, то проверяем на флеши и стриты"""
        komb, karta, name, car = complicated_ones(def_cards)
        if komb > local_komb:
            return komb, karta, name, car
        else:
            return local_komb, local_karta, local_name, local_cards


"""
старшая карта - 0
пара - 1
две пары - 2
сет (тройка) - 3
стрит - 4
флеш - 5
фулл хаус - 6
каре - 7
стрит флеш - 8
роял стрит флеш - 9
"""


def combo_solver(lists):
    cards = lists.copy()
    if len(cards) != 0:
        return simple_ones(cards)
    else:
        return -1, [], None, []