from pygame.locals import *
import pygame
from combo import combo_solver


class Player(pygame.sprite.Sprite):
    def __init__(self, name, x, y, image, width, height, card, money, ind, bit_coords, pocket_coords, name_coords):
        super().__init__()
        self.name = name
        self.card = card
        self.kombo_val, self.kombo, self.kombo_name, self.non_kombo = combo_solver(self.card)
        self.money = money
        self.bit = 0
        self.ind = ind
        self.is_my_turn = False
        self.is_done = False # сделал ход

        self.is_pushed = False
        self.is_rised = False
        self.is_passed = False
        self.is_taxed = False
        self.pic = pygame.image.load(image)
        self.image = pygame.transform.scale(self.pic, (width, height))

        self.pocket_coords = pocket_coords
        self.bit_coords = bit_coords
        self.name_coords = name_coords

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def cards_check(self):
        self.kombo_val, self.kombo, self.kombo_name, self.non_kombo = combo_solver(self.card)


class Cards(pygame.sprite.Sprite):
    """Класс кард с полями масть, значение, рубашка и изображение"""
    def __init__(self, suit, value, width, height, x=50, y=50):
        super().__init__()

        self.suit = suit
        self.value = value
        self.width = width
        self.height = height
        self.shirt = pygame.transform.scale(pygame.image.load(r"cards/backs/back1.png"), (self.width, self.height))
        self.image = pygame.transform.scale(pygame.image.load(r"cards/" + str(self.value) + str(self.suit) + ".png"),
                                            (self.width, self.height))

        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x


class Shirt(pygame.sprite.Sprite):
    """класс дял рубашек карт на столе"""
    def __init__(self, x, y, width, height, image, angle):
        super().__init__()
        self.image1 = pygame.transform.rotate(pygame.image.load(image), angle)
        self.image = pygame.transform.scale(self.image1, (width, height))

        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x
        self.angle = angle


class Button:
    """класс для кнопки"""
    def __init__(self, x, y, width, height, text, display, name='button'):

        self.x = x
        self.y = y
        self.height = height
        self.width = width
        self.text = text
        self.button_rect = Rect(self.x, self.y, self.width, self.height)
        self.display = display
        self.font = pygame.font.SysFont('font/Britanic.ttf', 30)

        self.pressed_color = (118, 1, 4)
        self.active_color = (168, 2, 19)
        self.terget_color = (226, 14, 19)

        self.disabled = False
        self.name = name

    def draw(self, color):
        pygame.draw.rect(self.display, color, self.button_rect)

    def shadow(self, color=(25, 255, 255)):
        pygame.draw.line(self.display, color, (self.x, self.y), (self.x + self.width, self.y), 4)
        pygame.draw.line(self.display, color, (self.x, self.y), (self.x, self.y + self.height), 4)
        pygame.draw.line(self.display, color, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), 4)
        pygame.draw.line(self.display, color, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), 4)

    def show(self, clicked):
        """функция для отрисовки"""
        action = False

        if not self.disabled:
            pos = pygame.mouse.get_pos()
            if self.button_rect.collidepoint(pos):
                if pygame.mouse.get_pressed()[0]:
                    clicked = True
                    """нажатие"""
                    self.draw(self.pressed_color)
                elif not pygame.mouse.get_pressed()[0] and clicked:
                    clicked = False
                    action = True
                else:
                    """не нажато, но наведено"""
                    self.draw(self.terget_color)
            else:
                """даже не наведено"""
                self.draw(self.active_color)
        else:
            """кнопка диактивирована"""
            self.draw(self.pressed_color)

        """добавление текста"""
        text_img = self.font.render(self.text, True, (0, 0, 0))
        text_len = text_img.get_width()
        text_tall = text_img.get_height()
        self.display.blit(text_img, (self.x + int(self.width / 2) - int(text_len / 2), self.y + int(self.height / 2) - int(text_tall / 2)))
        return action, clicked