import pygame
from classes import *
from combo import *
from random import randint, shuffle
from tkinter.simpledialog import askstring
from tkinter import Tk, messagebox
import pygame_menu


names = []
IPs = []
schet = [2000]
""" инициализания модуля pygame """
pygame.init()
display = pygame.display.set_mode((1200, 700))
pygame.display.set_caption('Абазар')
FPS = 60
clock = pygame.time.Clock()

# J - валет Q - дама K - король A - туз
numbers = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
# C - крести D - бубны H - черви S - пики
suits = ['C', 'D', 'H', 'S']


def main_game():
    done = False

    def finish(all_players):
        max_k = -1
        for i in all_players:
            if i.kombo_val > max_k:
                max_k = i.kombo_val
        kol = 0
        pl = []
        for i in all_players:
            if i.kombo_val == max_k:
                kol += 1
                pl.append(i)

        if kol > 1:
            max_v = 0
            for i in pl:
                if max_k == 3:
                    a = []
                    for j in i.kombo:
                        a.append(j)
                    b = max_cards(a)
                    integ = from_str_to_int(b)[0]
                elif max_k == 0:
                    b = i.kombo
                    integ = from_str_to_int([b.value])[0]
                else:
                    b = i.kombo
                    b, g = higest_card(b)
                    integ = from_str_to_int([b.value])[0]
                if integ > max_v:
                    max_v = integ
            kol_v = 0
            pl_v = []
            for i in all_players:
                if max_k == 3:
                    a = []
                    for j in i.kombo:
                        a.append(j)
                    b = max_cards(a)
                    integ = from_str_to_int([b])[0]
                elif max_k == 0:
                    a = i.kombo
                    h, trash = higest_card([a])
                    integ = from_str_to_int([h.value])[0]
                else:
                    a = i.kombo
                    h, trash = higest_card(a)
                    integ = from_str_to_int([h.value])[0]
                if integ == max_v:
                    kol_v += 1
                    pl_v.append(i)
            if kol_v > 1:
                max_n = 0
                for i in all_players:
                    d, e = higest_card(i.non_kombo)
                    d = from_str_to_int([d.value])[0]
                    if d > max_n:
                        max_n = d
                kol_n = 0
                pl_n = []
                for i in all_players:
                    d, e = higest_card(i.non_kombo)
                    d = from_str_to_int([d.value])[0]
                    if max_n == d:
                        pl_n.append(i)
                        kol_n += 1
                return pl_n

            elif kol_v == 1:
                return pl_v

        elif kol == 1:
            return pl

    def mixing(cards):
        """Функция удаления карты из списка всех карт и возвращения его"""
        i = randint(0, len(cards) - 1)
        k = cards[i]
        cards = cards[:i] + cards[(i + 1):]
        return k, cards

    def distribution(cards):
        """Выдаёт игроку 5 карт"""
        y = 500
        x = 260
        step = 125
        for i in range(5):
            card1, cards = mixing(cards)
            card1.rect.x = x + step * i
            card1.rect.y = y
            drown_list.add(card1)
        return cards

    def enemy_cards(lists):
        y = 10
        x = 470
        step = 125
        for i in range(5):
            card1 = lists[i]
            card1.rect.x = x + step * i
            card1.rect.y = y
            drown_list.add(card1)

    def cards_for_icon(cards):
        chosen_cards = []
        for i in range(5):
            card1, cards = mixing(cards)
            chosen_cards.append(card1)
        return chosen_cards, cards

    def check_rate():
        """Спрашивает размер ставки и возвращает это значение"""
        root = Tk()  # Создаем главное окно
        root.withdraw()  # скрываем его
        rate = askstring('Ставка', 'Сколько вы хотите поставить?')
        return rate

    def backs_on_table(drown_backs_list):

        """верхние карты"""
        for i in range(0, 200, 40):
            ps = 475
            shirt = Shirt(ps + i, -70, 125, 179, 'cards/backs/back1.png', 180)
            drown_backs_list.add(shirt)

        return drown_backs_list

    global names, IPs, schet

    """список всех карт"""
    cards = []
    dropped_cards = []

    try:
        name = names[len(names) - 1]  # Тут храниться имя игрока
        names.clear()
        print(name)
        names.append(name)
    except IndexError:
        root = Tk()  # Создаем главное окно
        root.withdraw()  # скрываем его
        messagebox.showwarning("Важно", "За Погосяна может играть только Погосян")
        name = 'клон Погосяна'
        print(name)
        names.append(name)

    print(names, IPs)
    # Заполнение класса карт
    for sui in suits:
        for num in numbers:
            cards.append(Cards(sui, num, 125, 179))
    shuffle(cards)  # Перемешивание колоды

    """отрисовываемые карты"""
    drown_list = pygame.sprite.Group()
    """отрисовка рубашек"""
    drown_backs_list = pygame.sprite.Group()
    """отрисовываем иконки(игроков)"""
    drown_icons = pygame.sprite.Group()

    deck_list = pygame.sprite.Group()

    """создание стопки карт"""
    for i in range(15, 9, -1):
        shirt = Shirt(i, i, 125, 179, 'cards/backs/back1.png', 0)
        deck_list.add(shirt)

    """кнопки"""
    game_buttons = []
    call_button = Button(900, 600, 80, 40, 'Call', display)
    game_buttons.append(call_button)
    rise_button = Button(990, 600, 80, 40, 'Rise', display)
    game_buttons.append(rise_button)
    pass_button = Button(1080, 600, 80, 40, 'Pass', display)
    game_buttons.append(pass_button)
    change_button = Button(900, 550, 260, 40, 'Change cards', display)
    game_buttons.append(change_button)
    start_button = Button(450, 320, 300, 60, 'Start', display)
    ok_button = Button(900, 500, 60, 40, 'Ok', display)
    # plus10_button = Button(450, 220, 300, 60, '+10', display)

    exit_button = Button(500, 350, 200, 70, 'Exit', display)

    """кнопки для смены карт"""
    cards_buttons = []
    card1_button = Button(260, 500, 125, 179, 'choose', display, 0)
    cards_buttons.append(card1_button)
    card2_button = Button(385, 500, 125, 179, 'choose', display, 1)
    cards_buttons.append(card2_button)
    card3_button = Button(510, 500, 125, 179, 'choose', display, 2)
    cards_buttons.append(card3_button)
    card4_button = Button(635, 500, 125, 179, 'choose', display, 3)
    cards_buttons.append(card4_button)
    card5_button = Button(760, 500, 125, 179, 'choose', display, 4)
    cards_buttons.append(card5_button)
    for i in cards_buttons:
        i.pressed_color = (0, 0, 0, 0)
        i.active_color = (0, 0, 0, 0)
        i.terget_color = (0, 0, 0, 0)

    all_players = []
    all_icons = []
    """игроки"""
    # icon1 = Player('игрок 1', 115, 300, 'cards/icons/icon1.jpg', 80, 80, [], 2000, 0, (150, 410), (130, 390), (115, 280))
    # all_icons.append(icon1)
    # all_players.append(icon1)

    icon1 = Player('игрок 2', 380, 20, 'cards/icons/icon2.jpg', 80, 80, [], 2000, 0, (415, 120), (395, 100), (380, 0))
    all_icons.append(icon1)
    all_players.append(icon1)

    # icon3 = Player('игрок 3', 1000, 300, 'cards/icons/icon3.jpg', 80, 80, [], 2000, 2, (1035, 410), (1015, 390), (1000, 280))
    # all_icons.append(icon3)
    # all_players.append(icon3)

    player = Player(names[0], -100, -100, 'cards/icons/icon1.jpg', 80, 80, [], schet[0], 1, (380, 470), (260, 460), (260, 440))
    all_players.append(player)

    coin = Shirt(185, 370, 30, 30, 'cards/coin.png', 0)
    # 185, 290 | 450, 10 | 1070, 290 | 420, 445
    drown_backs_list.add(coin)
    """нажатие кнопок"""
    call_clicked = False
    exit_clicked = False
    rise_clicked = False
    pass_clicked = False
    change_clicked = False
    start_clicked = False
    ok_clicked = False
    # plus10_clicked = False

    card_clicked = [False, False, False, False, False]

    font_bank = pygame.font.SysFont('font/Britanic.ttf', 66)
    font_name = pygame.font.SysFont('font/Britanic.ttf', 30)
    font_pocket = pygame.font.SysFont('font/Britanic.ttf', 20)
    font_player = pygame.font.SysFont('font/Britanic.ttf', 40)
    font_bit = pygame.font.SysFont('font/Britanic.ttf', 20)
    font_win = pygame.font.SysFont('font/Britanic.ttf', 120)
    bank_color = (236, 236, 36)
    bit_color = (64, 0, 5)
    win_color = (255, 15, 15)

    cur_turn = 0
    choosing = False
    started = False
    taxed = False
    firdt_time = True
    choosed = []
    bank = 0
    cur_bit = 0
    loop = 1
    time = 0
    turn_time = 1000
    winner = None

    """главный цикл игры"""
    while not done:
        call_action = False
        exit_action = False
        rise_action = False
        pass_action = False
        change_action = False
        ok_action = False
        card_action = [False, False, False, False, False]

        """отрисовка фона"""
        display.fill((00, 94, 00))

        """отрисовка кнопки"""
        if not started:

            started = True
            for i in all_icons:
                drown_icons.add(i)
            """обманки-рубашки в руки противников"""
            drown_backs_list = backs_on_table(drown_backs_list)
            """карты нам в руки"""
            cards = distribution(cards)
            for i in drown_list:
                player.card.append(i)
            """карты им в руки"""
            for i in drown_icons:
                i.card, cards = cards_for_icon(cards)

        else:
            call_action, call_clicked = call_button.show(call_clicked)
            rise_action, rise_clicked = rise_button.show(rise_clicked)
            pass_action, pass_clicked = pass_button.show(pass_clicked)
            change_action, change_clicked = change_button.show(change_clicked)
            bank_img = font_bank.render(str(bank), True, bank_color)
            display.blit(bank_img, (550, 250))

            for icon in drown_icons:
                pocket = font_pocket.render(str(icon.money), True, bank_color)
                display.blit(pocket, icon.pocket_coords)
                bit = font_bit.render(str(icon.bit), True, bit_color)
                display.blit(bit, icon.bit_coords)
                name = font_name.render(str(icon.name), True, (255, 255, 255))
                display.blit(name, icon.name_coords)

            pocket_player = font_player.render(str(player.money), True, bank_color)
            display.blit(pocket_player, (260, 460))
            bit_player = font_bit.render(str(player.bit), True, bit_color)
            display.blit(bit_player, (380, 470))
            name = font_name.render(str(player.name), True, (255, 255, 255))
            display.blit(name, player.name_coords)

            tax = 10
            for i in all_players:
                if i.is_taxed:
                    continue
                else:
                    bank += tax
                    i.money -= tax
                    pygame.time.delay(300)
                    i.is_taxed = True
                    break
            t = 0
            for i in all_players:
                if i.is_taxed:
                    t += 1
                if t == 2:
                    taxed = True

        if taxed:
            for person in all_players:
                print(cur_turn)
                if cur_turn == 1:
                    if not player.is_passed:
                        if player.bit < cur_bit:
                            player.is_my_turn = True

                            if cur_turn == person.ind:
                                for i in person.card:
                                    print(i.suit, i.value)
                                print(person.kombo_name)
                                if call_action:
                                    print('call')
                                    person.bit += cur_bit
                                    person.money -= cur_bit
                                    bank += cur_bit
                                    person.is_done = True
                                if rise_action:
                                    print('rise')
                                    bit = check_rate()
                                    print(bit, 'bibibit')
                                    print(type(bit), 'bibibit')
                                    while int(bit) < cur_bit:
                                        bit = check_rate()
                                    bit = int(bit)
                                    if bit > cur_bit:
                                        cur_bit = bit
                                    person.bit += cur_bit
                                    person.money -= cur_bit
                                    bank += cur_bit
                                    person.is_done = True
                        else:
                            player.is_done = True
                    else:
                        player.is_done = True
                else:
                    player.is_my_turn = False
                    if cur_turn == person.ind:
                        if time < turn_time:
                            time += 1
                        else:
                            person.cards_check()
                            print(person.name, person.kombo_val)
                            if not person.is_done:
                                if person.kombo_val in [0, 1, 2]:
                                    for i in person.card:
                                        print(i.suit, i.value)
                                    if cur_bit == 0:
                                        for i in person.non_kombo:
                                            old_card = i
                                            new_card, cards = mixing(cards)
                                            new_card.rect.x = old_card.rect.x
                                            new_card.rect.y = old_card.rect.y
                                            person.card.remove(old_card)
                                            person.card.append(new_card)
                                            dropped_cards.append(old_card)

                                    for i in person.card:
                                        print(i.suit, i.value)
                                    if cur_bit == 0:
                                        cur_bit = 50
                                        person.bit += cur_bit
                                        person.money -= cur_bit
                                        bank += cur_bit
                                    else:
                                        pl_bit = cur_bit - person.bit
                                        person.bit += pl_bit
                                        person.money -= pl_bit
                                        bank += pl_bit
                                        cur_bit = person.bit
                                elif person.kombo_val in [3, 4, 5]:
                                    if cur_bit == 0:
                                        cur_bit = 100
                                        person.bit += cur_bit
                                        person.money -= cur_bit
                                        bank += cur_bit
                                    else:
                                        pl_bit = cur_bit - person.bit
                                        person.bit += pl_bit
                                        person.money -= pl_bit
                                        bank += pl_bit
                                        cur_bit = person.bit
                                elif person.kombo_val > 0:
                                    if cur_bit == 0:
                                        cur_bit = 200
                                        person.bit += cur_bit
                                        person.money -= cur_bit
                                        bank += cur_bit
                                    else:
                                        pl_bit = cur_bit - person.bit
                                        person.bit += pl_bit
                                        person.money -= pl_bit
                                        bank += pl_bit
                                        cur_bit = person.bit
                                person.is_done = True

                            time = 0
                            for i in person.card:
                                print(i.suit, i.value)
                            print(person.kombo_name)
                            cur_turn += 1
        #
        # c = 0
        # for i in all_players:
        #     if i.bit == cur_bit and cur_bit != 0:
        #     # if i.bit == cur_bit:
        #         c += 1
        #     elif loop == 2:
        #         i.is_pushed = False
        # if c == 4:
        #     finish(all_players)
        # elif player.is_pushed:
        #     if cur_turn == 3:
        #         loop += 1
        #         cur_turn = 0
        print(cur_bit, 'bbbbbbbbbiiiiiiittttt')
        for i in all_players:
            print(i.bit)
        if player.is_done:
            cur_turn = 0
        if cur_turn == 0:
            coin.rect.x = 450
            coin.rect.y = 90
            # 185, 290 | 450, 10 | 1070, 290 | 420, 445
        if cur_turn == 1:
            coin.rect.x = 420
            coin.rect.y = 445
        d = 0
        b = 0

        for i in all_players:
            if i.is_done:
                d += 1
            if i.bit == cur_bit:
                b += 1
        print('b', b, 'd', d, 'loop', loop)
        if d == 2:
            if b == 2:
                for i in all_players:
                    i.cards_check()
                    print(i.kombo_name)
                print('finisk')
                winner = finish(all_players)
                print(winner)
                for i in winner:
                    print(i.name, i.kombo_name)
                player.is_my_turn = False
                if len(winner) == 1:
                    if winner[0].ind == 1:
                        win_banner = font_win.render("you win", True, win_color)
                        player.money += bank
                    else:
                        win_banner = font_win.render("you Lose", True, win_color)
                        icon1.money += bank
                else:
                    win_banner = font_win.render("Tie", True, win_color)
                    icon1.money += int(bank/2)
                    player.money += int(bank/2)
                for i in all_players:
                    i.bit = 0
                cur_bit = 0
                bank = 0
                schet[0] = player.money
                enemy_cards(icon1.card)
                for i in drown_backs_list:
                    drown_backs_list.remove(i)
                display.blit(win_banner, (450, 260))
                exit_action, exit_clicked = exit_button.show(exit_clicked)
                if exit_action:
                    done = True
            else:
                for i in all_players:
                    i.is_done = False
                loop += 1
        if player.is_passed:
            win_banner = font_win.render("you Lose", True, win_color)
            icon1.money += bank
            for i in all_players:
                i.bit = 0
            cur_bit = 0
            bank = 0
            schet[0] = player.money
            enemy_cards(icon1.card)
            for i in drown_backs_list:
                drown_backs_list.remove(i)
            display.blit(win_banner, (450, 260))
            exit_action, exit_clicked = exit_button.show(exit_clicked)
            if exit_action:
                done = True

        if choosing:
            ok_action, ok_clicked = ok_button.show(ok_clicked)

            for i in range(5):
                card_but = None
                for but in cards_buttons:
                    if but.name == i:
                        card_but = but
                card_action[i], card_clicked[i] = card_but.show(card_clicked[i])
                if card_action[i]:
                    if card_but in choosed:
                        choosed.remove(card_but)
                    else:
                        choosed.append(card_but)
                if card_but in choosed:
                    card_but.shadow((0, 0, 255))
                else:
                    card_but.shadow()

        if not player.is_my_turn:
            for i in game_buttons:
                i.disabled = True
        else:
            for i in game_buttons:
                i.disabled = False
        if pass_action:
            print('pass')
            for i in game_buttons:
                i.disabled = True
            player.is_passed = True
            player.is_done = True
        if change_action:
            if choosing:
                choosing = False
            else:
                choosing = True
            print('change')

        if ok_action:
            if len(choosed) != 0:
                choosing = False
                change_button.disabled = True
                for i in choosed:
                    print(i.name, 'ooooooookkkkkkk')
                    old_card = player.card[i.name]
                    print(old_card.suit, old_card.value)
                    new_card, cards = mixing(cards)
                    print(new_card.suit, new_card.value)
                    new_card.rect.x = old_card.rect.x
                    new_card.rect.y = old_card.rect.y
                    player.card[i.name] = new_card
                    drown_list.add(new_card)
                    drown_list.remove(old_card)
                    dropped_cards.append(old_card)

        """обработка событий"""
        for e in pygame.event.get():
            # print(e)
            if e.type == pygame.QUIT:
                done = True

        """обновление всех обьектов"""
        drown_list.update()
        drown_backs_list.update()

        """отрисовка всех обьектов"""

        drown_backs_list.draw(display)
        deck_list.draw(display)
        drown_icons.draw(display)
        drown_list.draw(display)

        """обновление экрана"""
        pygame.display.flip()
        clock.tick(FPS)




def check_name_test(value: str):
    global names
    names.append(value)


# def check_IP_test(value: str):
#     global IPs
#     IPs.append(value)


main_theme = pygame_menu.themes.THEME_GREEN.copy()
main_theme.set_background_color_opacity(0.2)
menu = pygame_menu.Menu('Добро пожаловать', 400, 300, theme=main_theme)

menu.add.text_input('Имя: ', default='Погосян', onchange= check_name_test)
# menu.add.text_input('IP: ', onchange=check_IP_test)
menu.add.button('Играть', main_game)
menu.add.button('Выход', pygame_menu.events.EXIT)
bg_image = pygame.image.load('cards/Fon.jpg')

while True:

    display.blit(bg_image, (0, 0))

    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            exit()

    if menu.is_enabled():
        menu.update(events)
        menu.draw(display)

    pygame.display.update()

